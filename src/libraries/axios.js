import axios from 'axios';
const URL = "https://api.yelp.com/v3";
const HEADER = "Bearer Ubf1-f0uqsJUnssqPMGo-tiFeZTT85oFmKfznlPmjDtX8s83jYMoAb-ApuD63wgq6LDZNsUXG6gurZIVYaj2jzxJmmLdCdXbDqIHU_b6KiCEVi8v-YB0OSsW6MWaY3Yx";

class Axios {

    static getRestApiWithToken = async (endpoint) => {
        console.log(URL + endpoint)
        const header = {
            'Authorization': HEADER,
        };
        const config = {
            headers: header,
          };
        
        console.log(config);
        const returnStatus = await axios.get( URL + endpoint, config)
            .then(function (response) {
                console.log(response);
                return { response : response };
            })
            .catch(function (error) {
                console.log(error)
                return { response: error.response };
            });
        return returnStatus;
    }
    
}
export default Axios;
