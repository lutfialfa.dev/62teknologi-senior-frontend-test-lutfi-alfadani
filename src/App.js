// App.js
import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import Axios from './libraries/axios';
import { endpoint } from './libraries/endpoint';

async function App() {
  const send = await Axios.getRestApiWithToken(endpoint.getSearch);
  console.log(send);
  return (
    <div className="App">
      <h5>React Bootstrap Table Next with Sorting and Pagination</h5>

      
    </div>
  );
}

export default App;
